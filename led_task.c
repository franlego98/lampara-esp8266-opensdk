#include "led_task.h"

extern uint8_t neopix_gamma[];
ws2812_pixel_t pixels[LED_NUMBER];
extern int leds_mode;
uint64_t t;

inline uint8_t neopix(uint8_t c){
    return neopix_gamma[(uint8_t)c];
    
}
//#define NEOPIX(c) (neopix_gamma[(uint8_t)c])

void led_task(void *pvParameters)
{
    ws2812_i2s_init(LED_NUMBER, PIXEL_RGB);
	
	for(int i = 0; i < LED_NUMBER;i++){
		pixels[i].red = neopix_gamma[255];
		pixels[i].green = neopix_gamma[150];
		pixels[i].blue = 0;
		pixels[i].white = 0;
	}
		
    while (1) {
		switch(leds_mode){
			case 0:
				break;
			case 1:
				for(int i = 0; i < LED_NUMBER;i++){
					hsv color1 = { t % 360 , 1.0 , 1.0 };
					rgb color2 = hsv2rgb(color1);
					pixels[i].red = neopix(color2.r*255.0);
					pixels[i].green = neopix(color2.g*255.0);
					pixels[i].blue = neopix(color2.b*255.0);
				}
				break;
			case 2:
				for(int i = 0; i < LED_NUMBER;i++){
					hsv color1 = { ((t + i*10) % 360) , 1.0 , 1.0 };
					rgb color2 = hsv2rgb(color1);
					pixels[i].red = neopix(color2.r*255.0);
					pixels[i].green = neopix(color2.g*255.0);
					pixels[i].blue = neopix(color2.b*255.0);
				}
				break;
		}
		
        ws2812_i2s_update(pixels, PIXEL_RGB);
        t++;
        vTaskDelay(20 / portTICK_PERIOD_MS);
    }
}
