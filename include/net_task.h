#ifndef __NET_TASK_H__
#define __NET_TASK_H__

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/api.h"
#include <string.h>
#include <stdio.h>
#include "parameters.h"
#include "ws2812_i2s/ws2812_i2s.h"
#include "colors.h"

#include "rboot-api.h"
#include "rboot-ota/ota-tftp.h"

void net_client_task(int c_fd);
void net_task(void *args);

#endif
