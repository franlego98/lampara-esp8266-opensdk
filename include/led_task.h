#ifndef __LED_TASK_H_
#define __LED_TASK_H_

#include "ws2812_i2s/ws2812_i2s.h"
#include "colors.h"
#include <FreeRTOS.h>
#include <task.h>
#include "parameters.h"
#include <espressif/esp_common.h>
#include <stdio.h>

void led_task(void *pvParameters);

#endif
