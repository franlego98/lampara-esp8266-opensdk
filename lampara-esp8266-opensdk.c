#include <espressif/esp_common.h>
#include <esp8266.h>
#include <esp/uart.h>
#include <string.h>
#include <stdio.h>
#include <FreeRTOS.h>
#include <math.h>
#include <task.h>
#include <ssid_config.h>

#include "net_task.h"
#include "led_task.h"

int leds_mode;

void user_init(){
	uart_set_baud(0, 115200);
    printf("SDK version:%s\n", sdk_system_get_sdk_version());

    struct sdk_station_config config = {
        .ssid = WIFI_SSID,
        .password = WIFI_PASS,
    };
    
    /* required to call wifi_set_opmode before station_set_config */
    sdk_wifi_set_opmode(STATION_MODE);
    sdk_wifi_station_set_config(&config);
	sdk_wifi_station_connect();
	
	printf("main task freeHeap: %u\n",xPortGetFreeHeapSize());
	xTaskCreate(net_task,"net_task",2048,NULL,10,NULL);
	xTaskCreate(led_task,"led_task", 1024, NULL, configMAX_PRIORITIES, NULL);
}
