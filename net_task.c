#include "net_task.h"

extern neopix_gamma[];
extern ws2812_pixel_t pixels[LED_NUMBER];
extern int leds_mode;

void net_client_task(int c_fd){
	static int current_cons;
	current_cons++;
	char buffer[50];
	char cmd[25];
	char arg[25];
	cmd[0] = 0;
	arg[0] = 0;

	char banner[] = "LAMPARA ESP8266 V2 OK!\n";
	lwip_write(c_fd,banner,strlen(banner));

	
	while(1){
		int n = read(c_fd,buffer,sizeof(buffer)-1);
		if(n < 0) break;
		buffer[n] = 0;
		sscanf(buffer,"%s %s\n",cmd,arg);
    
		if(strcmp(cmd,"fill") == 0){
			leds_mode = 0;
            uint16_t r,g,b;
			if(sscanf(arg,"%hu,%hu,%hu",&r,&g,&b) == 3){
				for(int i = 0; i < LED_NUMBER;i++){
					pixels[i].red = neopix_gamma[r];
					pixels[i].green = neopix_gamma[g];
					pixels[i].blue = neopix_gamma[b];
				}
				write(c_fd,"OK\n",3);		
			}else{
				write(c_fd,"BAD VALUES\n",11);				
			}
        } else if(strcmp(cmd,"inc") == 0){
            int16_t r,g,b;
            
            leds_mode = 0;
			if(sscanf(arg,"%hi,%hi,%hi",&r,&g,&b) == 3){
				for(int i = 0; i < LED_NUMBER;i++){
					int16_t nuevo_r = pixels[i].red+(r*256)/100;
                    if(nuevo_r > 255) pixels[i].red=255;
                    else if(nuevo_r < 0) pixels[i].red=0;
                    else pixels[i].red+=(r*256)/100;
                    
					int16_t nuevo_g = pixels[i].green+(g*256)/100;
                    if(nuevo_g > 255) pixels[i].green=255;
                    else if(nuevo_g < 0) pixels[i].green=0;
                    else pixels[i].green+=(g*256)/100;
                    
					int16_t nuevo_b = pixels[i].blue+(b*256)/100;
                    if(nuevo_b > 255) pixels[i].blue=255;
                    else if(nuevo_b < 0) pixels[i].blue=0;
                    else pixels[i].blue+=(b*256)/100;
                    
				}
                
				write(c_fd,"OK\n",3);		
			}else{
				write(c_fd,"BAD VALUES\n",11);				
			}
		} else if(strcmp(cmd,"frame") == 0){
			int count = 0;
			while(count < sizeof(pixels)) {
				count+=read(c_fd,pixels,sizeof(pixels) - count);
			}
				
			/*if(count == LED_NUMBER*4)
				write(c_fd,"OK\n",3);		
			else
				write(c_fd,"BAD VALUES\n",11);				*/
		
		} else if(strcmp(cmd,"mode") == 0){
			int error = 0;
			
			if(strcmp(arg,"static") == 0 || atoi(arg) == 0){
				leds_mode = 0;
			} else if(strcmp(arg,"rainbow") == 0 || atoi(arg) == 1){
				leds_mode = 1;
			} else if(strcmp(arg,"fullrainbow") == 0 || atoi(arg) == 2){
				leds_mode = 2;
			} else {
				error = 1;
			}
			
			if(error == 0)
				write(c_fd,"OK\n",3);
			else
				write(c_fd,"UNKNOWN MODE\n",13);				
            
        } else if(strcmp(cmd,"current") == 0){
            if(leds_mode == 0){
                sprintf(buffer,"%u,%u,%u\n",
                        pixels[0].red,
                        pixels[0].green,
                        pixels[0].blue);
            }else{
                strcpy(buffer,"NOT IN STATIC MODE\n");
            }
            write(c_fd,buffer,strlen(buffer));
		} else if(strcmp(cmd,"close") == 0){
			write(c_fd,"OK\n",3);				
			break;
		} else if(strcmp(cmd,"ota") == 0){
			ota_tftp_init_server(TFTP_PORT);
			write(c_fd,"UPLOAD NOW!\n",12);			
			break;
		} else if(strcmp(cmd,"reset") == 0){
			write(c_fd,"OK\n",3);			
			sdk_system_restart();
		} else if(strcmp(cmd,"status") == 0) {
			sprintf(buffer,"Free Heap Memory: %u bytes\n",xPortGetFreeHeapSize());
			write(c_fd,buffer,strlen(buffer));
			sprintf(buffer,"Active connections: %u\n",current_cons);
			write(c_fd,buffer,strlen(buffer));
			write(c_fd,"OK\n",3);
		} else {
			write(c_fd,"UNKNOWN\n",8);				
		}
		
	}
	close(c_fd);
	
	current_cons--;
	vTaskDelete(NULL);
}

void net_task(void *args){
	while(1){
		printf("task1 top freeHeap: %u\n",xPortGetFreeHeapSize());
		
		int s_fd = socket(AF_INET, SOCK_STREAM, 0);
		LWIP_ASSERT("s_fd >= 0", s_fd >= 0);
		
		struct sockaddr_in sa,isa;
		size_t addr_size;
		
		memset(&sa, 0, sizeof(struct sockaddr_in));
		sa.sin_family = AF_INET;
		sa.sin_addr.s_addr = inet_addr("0.0.0.0");
		sa.sin_port = htons(8000);
		
		int binded = bind(s_fd,(struct socketaddr *) &sa,sizeof(sa));
		LWIP_ASSERT("binded == 0", binded == 0);
		
		int listening = listen(s_fd,0);
		LWIP_ASSERT("listening == 0", listening == 0);
		
		while(1){
			printf("task1 accept freeHeap: %u\n",xPortGetFreeHeapSize());
			
			int client_fd = accept(s_fd,(struct sockaddr*)&isa,&addr_size);
			LWIP_ASSERT("client_fd >= 0", client_fd >= 0);
			
			if(client_fd < 0) break;
			
			xTaskCreate(net_client_task,"net_client_task",1024,client_fd,configMAX_PRIORITIES,NULL);
			
		}
	}
}
